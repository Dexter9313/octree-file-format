::############################################################################
::# Install ZSTD
::############################################################################
IF NOT EXIST zstd\ (
	git clone https://github.com/facebook/zstd.git --branch=v1.5.6

	cd zstd\build\cmake
	mkdir build
	cd build
	cmake .. -G "Visual Studio 16 2019" -A x64 -DCMAKE_BUILD_TYPE=Release
	cmake --build . --config Release

	cd lib\Release
	dir
	cd ..\..

	cd ..\..\..\..
	echo "ZSTD installed..."
)
set ZSTD_INCLUDE_DIRS=%APPVEYOR_BUILD_FOLDER%/deps/zstd/lib/
set ZSTD_LIBRARIES=%APPVEYOR_BUILD_FOLDER%/deps/zstd/build/cmake/build/lib/Release/zstd_static.lib
::# set ZSTD_SHARED=%APPVEYOR_BUILD_FOLDER%\deps\zstd\build\cmake\build\lib\zstd.dll
echo "ZSTD..."
