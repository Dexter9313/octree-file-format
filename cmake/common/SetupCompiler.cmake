# C++ 20
include(CheckCXXCompilerFlag)
if (MSVC)
    # For MSVC, use the /std:c++20 flag
    set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} /std:c++20")
	set(CMAKE_CXX_FLAGS_RELEASE "/O2")
else()
    # For other compilers, check for the appropriate flags
    CHECK_CXX_COMPILER_FLAG("-std=gnu++20" COMPILER_SUPPORTS_GNU_CXX20)
    CHECK_CXX_COMPILER_FLAG("-std=c++20" COMPILER_SUPPORTS_CXX20)
    CHECK_CXX_COMPILER_FLAG("-std=c++2a" COMPILER_SUPPORTS_CXX2A)

    if(COMPILER_SUPPORTS_GNU_CXX20)
        set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -std=gnu++20")
    elseif(COMPILER_SUPPORTS_CXX20)
        set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -std=c++20")
    elseif(COMPILER_SUPPORTS_CXX2A)
        set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -std=c++2a")
    else()
        message(STATUS "The compiler ${CMAKE_CXX_COMPILER} has no C++20 support. Please use a different C++ compiler.")
    endif()
	set(CMAKE_CXX_FLAGS_RELEASE "-O3")
endif()

if(MSVC)
	set(MSVC_COMPILER_DEFS "-D_VARIADIC_MAX=10")
endif()

#WARNINGS
if(CMAKE_COMPILER_IS_GNUCC OR CMAKE_COMPILER_IS_GNUCXX)
	set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -Wall -Wextra -pedantic -Werror")
endif()
