/*
    Copyright (C) 2024 Florian Cabot <florian.cabot@hotmail.fr>

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#ifndef INPUTWIDGET_HPP
#define INPUTWIDGET_HPP

#include "inputwidgets/CommandInputParametersWidget.hpp"

class HDF5InputWidget;
class QDoubleSpinBox;
class QTabWidget;

class InputWidget : public CommandInputParametersWidget
{
	Q_OBJECT
  public:
	explicit InputWidget(QWidget* parent = nullptr);
	QStringList generateArguments() const override;

  protected slots:
	void evaluateValid() override;

  private:
	CommandInputParametersWidget const& getCurrentParamWidget() const;
	CommandInputParametersWidget& getCurrentParamWidget();

	QDoubleSpinBox* sampleRateSpinBox;
	QTabWidget* inputTypeSelector;
};

#endif // INPUTWIDGET_HPP
