/*
    Copyright (C) 2024 Florian Cabot <florian.cabot@hotmail.fr>

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#ifndef COMMANDPARAMETERSWIDGET_HPP
#define COMMANDPARAMETERSWIDGET_HPP

#include <QWidget>

class CommandParametersWidget : public QWidget
{
	Q_OBJECT
  public:
	explicit CommandParametersWidget(QWidget* parent = nullptr);
	bool isValid() const { return valid; };
	virtual QStringList generateArguments() const = 0;

  signals:
	void validityChanged(bool valid);

  protected:
	void setValidity(bool valid);

  protected slots:
	virtual void evaluateValid() = 0;

  private:
	bool valid = false;
};

#endif // COMMANDPARAMETERSWIDGET_HPP
