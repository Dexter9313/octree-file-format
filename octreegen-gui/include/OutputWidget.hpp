/*
    Copyright (C) 2024 Florian Cabot <florian.cabot@hotmail.fr>

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#ifndef OUTPUTWIDGET_HPP
#define OUTPUTWIDGET_HPP

#include "CommandParametersWidget.hpp"

class QCheckBox;
class QLabel;
class QLineEdit;
class QSpinBox;

class OutputWidget : public CommandParametersWidget
{
	Q_OBJECT
  public:
	explicit OutputWidget(QWidget* parent = nullptr);
	QStringList generateArguments() const override;

  public slots:
	void setOutputAutoPath(QString const& path);

  protected slots:
	void evaluateValid() override;

  private slots:
	void selectSaveAs();

  private:
	QString outputAutoPath;

	QCheckBox* disableNodeNormCB;
	QCheckBox* useZSTDCB;
	QSpinBox* maxParticlesPerNodeSB;

	QCheckBox* autoCheckBox;
	QLineEdit* lineEditSaveAs;
	QLabel* invalidityLabel;
};

#endif // OUTPUTWIDGET_HPP
