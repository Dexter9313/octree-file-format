/*
    Copyright (C) 2018 Florian Cabot <florian.cabot@epfl.ch>

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#include "MainWindow.hpp"

#include <QCoreApplication>
#include <QFileDialog>
#include <QHBoxLayout>
#include <QMessageBox>
#include <QProcess>
#include <QScrollBar>
#include <QTextEdit>
#include <QVBoxLayout>

#include "Console.hpp"
#include "utils.hpp"

MainWindow::MainWindow()
    : menubar(this)
    , centralWidget(this)
    , generateButton(tr("Generate octrees"), this)
    /*, coordinatesSelect(this, tr("Select particles coordinates :"))
    , radiusSelect(this, tr("Select particles radius (optional) :"))
    , luminositySelect(this, tr("Select particles luminosity (optional) :"))*/
    , inputWidget(this)
    , outputWidget(this)
{
	auto* menu = make_qt_unique<QMenu>(menubar, "File");
	connect(menu->addAction(tr("&Show Octree info...")), SIGNAL(triggered()),
	        this, SLOT(showOctreeInfo()));
	connect(menu->addAction(tr("&Quit")), SIGNAL(triggered()), this,
	        SLOT(close()));
	menubar.addMenu(menu);

	menu = make_qt_unique<QMenu>(menubar, "About");
	connect(menu->addAction(tr("&About octreegen-gui")), SIGNAL(triggered()),
	        this, SLOT(aboutOctreegenGUI()));
	connect(menu->addAction(tr("A&bout Qt")), SIGNAL(triggered()), this,
	        SLOT(aboutQt()));
	menubar.addMenu(menu);

	setMenuBar(&menubar);

	auto* mainLayout = make_qt_unique<QVBoxLayout>(centralWidget);
	mainLayout->addWidget(&inputWidget);
	connect(&inputWidget, &InputWidget::validityChanged, this,
	        &MainWindow::updateGenerateButton);
	connect(&inputWidget, &InputWidget::outputAutoPathChanged, &outputWidget,
	        &OutputWidget::setOutputAutoPath);

	/*auto* w0     = make_qt_unique<QWidget>(*this);
	auto* layout = make_qt_unique<QHBoxLayout>(*w0);

	layout->addWidget(&coordinatesSelect);
	layout->addWidget(&radiusSelect);
	layout->addWidget(&luminositySelect);

	mainLayout->addWidget(w0);

	connect(&coordinatesSelect, SIGNAL(selectedObjChanged()), this,
	        SLOT(updateGenerateButton()));
	connect(&radiusSelect, SIGNAL(selectedObjChanged()), this,
	        SLOT(updateGenerateButton()));
	connect(&luminositySelect, SIGNAL(selectedObjChanged()), this,
	        SLOT(updateGenerateButton()));*/

	mainLayout->addWidget(&outputWidget);
	outputWidget.setOutputAutoPath(inputWidget.getOutputAutoPath());
	connect(&outputWidget, &OutputWidget::validityChanged, this,
	        &MainWindow::updateGenerateButton);

	auto* bottom  = make_qt_unique<QWidget>(*this);
	auto* bottomL = make_qt_unique<QHBoxLayout>(*bottom);

	connect(&generateButton, SIGNAL(clicked(bool)), this, SLOT(generate()));
	bottomL->addWidget(&generateButton);

	mainLayout->addWidget(bottom);

	setCentralWidget(&centralWidget);
	// centralWidget.setEnabled(false);

	updateGenerateButton();
}

void MainWindow::showOctreeInfo()
{
	auto octreeFileName = QFileDialog::getOpenFileName(
	    this, tr("Open Octree File"), QString(""),
	    tr("Octree Files (*.octree)"));

	Console::run("octreegen", {"info", octreeFileName});
}

/*void MainWindow::openHDF5()
{
    fileNames = QFileDialog::getOpenFileNames(
        this, tr("Open HDF5 File(s)"), QString(""), tr("HDF5 Files (*.hdf5)"));
    if(fileNames.empty())
    {
        return;
    }
    coordinatesSelect.load(fileNames);
    radiusSelect.load(fileNames);
    luminositySelect.load(fileNames);
    centralWidget.setEnabled(true);

    QFileInfo fi(fileNames[0]);
    outputWidget.setOutputAutoPath(fi.dir().absolutePath() + QDir::separator()
                                   + fi.baseName() + ".octree");
}*/

void MainWindow::updateGenerateButton()
{
	generateButton.setEnabled(inputWidget.isValid() && outputWidget.isValid());
}

void MainWindow::generate()
{
	this->setEnabled(false);

	QString program("octreegen");
	QStringList arguments;
	arguments << "generate";
	for(auto const& arg : inputWidget.generateArguments())
	{
		arguments << arg;
	}
	/*if(radiusSelect.datasetPathIsValid())
	{
	    arguments << "--radius-path=" + radiusSelect.getDatasetPath();
	}
	if(luminositySelect.datasetPathIsValid())
	{
	    arguments << "--lum-path=" + luminositySelect.getDatasetPath();
	}*/
	for(auto const& arg : outputWidget.generateArguments())
	{
		arguments << arg;
	}

	Console::run(program, arguments);

	this->setEnabled(true);
}

void MainWindow::aboutOctreegenGUI()
{
	QMessageBox::about(
	    this, tr("About octreegen-gui"),
	    tr("Copyright (C) 2018 Florian Cabot <florian.cabot@epfl.ch>\n"
	       "\n"
	       "This program is free software; you can redistribute it and/or"
	       "modify\n"
	       "it under the terms of the GNU General Public License as published "
	       "by\n"
	       "the Free Software Foundation; either version 3 of the License, or"
	       "(at your option) any later version.\n"
	       "\n"
	       "This program is distributed in the hope that it will be useful,\n"
	       "but WITHOUT ANY WARRANTY; without even the implied warranty of\n"
	       "MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the\n"
	       "GNU General Public License for more details.\n"
	       "\n"
	       "You should have received a copy of the GNU General Public License "
	       "along\n"
	       "with this program; if not, write to the Free Software Foundation, "
	       "Inc.,\n"
	       "51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA."));
}

void MainWindow::aboutQt()
{
	QMessageBox::aboutQt(this);
}
