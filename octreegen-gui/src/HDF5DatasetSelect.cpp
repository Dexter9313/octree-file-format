/*
    Copyright (C) 2018 Florian Cabot <florian.cabot@epfl.ch>

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#include "HDF5DatasetSelect.hpp"

#include <QHBoxLayout>
#include <QVBoxLayout>

HDF5DatasetSelect::HDF5DatasetSelect(QString const& particlesLabel,
                                     int dimensions, QWidget* parent)
    : QWidget(parent)
    , tree(this)
    , lineEditDataset(this)
    , infoLabel(this)
    , dimensions(dimensions)
{
	auto* layoutV = make_qt_unique<QVBoxLayout>(*this);
	layoutV->addWidget(make_qt_unique<QLabel>(*this, particlesLabel));

	tree.setColumnCount(1);
	tree.setHeaderLabel(QString());
	connect(&tree, SIGNAL(itemSelectionChanged()), this, SLOT(fixLineEdit()));
	layoutV->addWidget(&tree);

	auto* w       = make_qt_unique<QWidget>(*this);
	auto* layoutH = make_qt_unique<QHBoxLayout>(*w);
	layoutH->addWidget(make_qt_unique<QLabel>(*this, tr("Dataset path :")));
	connect(&lineEditDataset, SIGNAL(textEdited(QString const&)), this,
	        SLOT(lineEdited(QString const&)));
	connect(&lineEditDataset, SIGNAL(editingFinished()), this,
	        SLOT(fixLineEdit()));
	layoutH->addWidget(&lineEditDataset);
	layoutV->addWidget(w);

	updateInfoLabel();
	layoutV->addWidget(&infoLabel);
}

bool HDF5DatasetSelect::datasetPathIsValid() const
{
	InvalidReason ignoredInvalidReason = InvalidReason::VALID;
	return datasetPathIsValid(ignoredInvalidReason);
}

bool HDF5DatasetSelect::datasetPathIsValid(InvalidReason& invalidReason) const
{
	invalidReason = InvalidReason::VALID;
	QString path(lineEditDataset.text());
	if(path == "")
	{
		invalidReason = InvalidReason::EMPTY_PATH;
		return false;
	}
	QStringList p(path.split('/'));
	HDF5Object const* result = nullptr;
	if(p.size() <= 1)
	{
		result = nullptr;
	}
	else if(p.size() > 1 && p[0] == "")
	{
		p.pop_front();
		result = &hdf5_obj;
		if(p[0] != "")
		{
			result = objFromPath(p, result);
		}
	}
	if(result != nullptr && result->type != H5O_TYPE_DATASET)
	{
		invalidReason = InvalidReason::NOT_DATASET;
		return false;
	}
	if(result->dims[1] != dimensions)
	{
		invalidReason = InvalidReason::DIMENSIONS_MISMATCH;
		return false;
	}
	return true;
}

void HDF5DatasetSelect::load(QStringList const& filePaths)
{
	tree.clear();
	hdf5_obj = readHDF5RootObject(filePaths[0].toStdString());
	bool ok  = false;
	for(int i(1); i < filePaths.size(); ++i)
	{
		hdf5_obj = intersection(
		    hdf5_obj, readHDF5RootObject(filePaths[i].toStdString()), ok);
		if(!ok)
		{
			return;
		}
	}
	// it is owned by Qt so not leaked
	auto* notLeaked = constructItems(hdf5_obj).release();
	(void) notLeaked;
	tree.setRootIsDecorated(true);
}

void HDF5DatasetSelect::lineEdited(QString const& path)
{
	if(path == "")
	{
		tree.clearSelection();
		return;
	}
	QStringList p(path.split('/'));
	QTreeWidgetItem* result = nullptr;
	if(p.size() <= 1)
	{
		result = nullptr;
	}
	else if(p.size() > 1 && p[0] == "")
	{
		p.pop_front();
		result = tree.topLevelItem(0);
		if(p[0] != "")
		{
			result = itemFromPath(p, result);
		}
	}
	if(result != nullptr)
	{
		tree.setCurrentItem(result);
	}
}

void HDF5DatasetSelect::fixLineEdit()
{
	if(tree.selectedItems().size() == 0)
	{
		lineEditDataset.setText("");
	}
	else
	{
		lineEditDataset.setText(pathFromItem(tree.selectedItems()[0]));
	}
	updateInfoLabel();

	emit selectedObjChanged();
}

std::unique_ptr<QTreeWidgetItem>
    HDF5DatasetSelect::constructItems(HDF5Object const& obj,
                                      QTreeWidgetItem* parent)
{
	std::unique_ptr<QTreeWidgetItem> item;
	if(parent == nullptr)
	{
		item = std::make_unique<QTreeWidgetItem>(&tree);
	}
	else
	{
		item = std::make_unique<QTreeWidgetItem>(parent);
	}

	item->setText(0, QString::fromStdString(obj.name));

	for(auto const& o : obj.links)
	{
		// addChild takes ownership
		item->addChild(constructItems(o, item.get()).release());
	}

	return item;
}

void HDF5DatasetSelect::updateInfoLabel()
{
	InvalidReason invalidReason = InvalidReason::VALID;
	if(datasetPathIsValid(invalidReason))
	{
		infoLabel.setStyleSheet("QLabel { color : green; }");
		infoLabel.setText(tr("Dataset selected"));
		return;
	}
	infoLabel.setStyleSheet("QLabel { color : red; }");
	switch(invalidReason)
	{
		case InvalidReason::EMPTY_PATH:
			infoLabel.setText(tr("No object selected"));
			break;
		case InvalidReason::NOT_DATASET:
			infoLabel.setText(tr("Selected object is not a dataset"));
			break;
		case InvalidReason::DIMENSIONS_MISMATCH:
			infoLabel.setText(tr("Selected dataset is not of dimension %1")
			                      .arg(QString::number(dimensions)));
			break;
		case InvalidReason::VALID:
		default:
			break;
	}
}

QString HDF5DatasetSelect::pathFromItem(QTreeWidgetItem* item)
{
	if(item->parent() != nullptr)
	{
		QString parentPath(pathFromItem(item->parent()));
		if(parentPath == "/")
		{
			return pathFromItem(item->parent()) + item->text(0);
		}
		return pathFromItem(item->parent()) + '/' + item->text(0);
	}
	return item->text(0);
}

QTreeWidgetItem* HDF5DatasetSelect::itemFromPath(QStringList& path,
                                                 QTreeWidgetItem* rootItem)
{
	if(path.size() == 0)
	{
		return rootItem;
	}
	for(int i(0); i < rootItem->childCount(); ++i)
	{
		QTreeWidgetItem* child = rootItem->child(i);
		if(child == nullptr)
		{
			continue;
		}
		if(child->text(0) == path[0])
		{
			if(path.size() == 1)
			{
				return child;
			}
			path.pop_front();
			return itemFromPath(path, child);
		}
	}

	return nullptr;
}

HDF5Object const* HDF5DatasetSelect::objFromPath(QStringList& path,
                                                 HDF5Object const* rootObj)
{
	if(path.size() == 0)
	{
		return rootObj;
	}
	for(auto& link : rootObj->links)
	{
		HDF5Object const* obj = &link;
		if(obj->name == path[0].toStdString())
		{
			if(path.size() == 1)
			{
				return obj;
			}
			path.pop_front();
			return objFromPath(path, obj);
		}
	}

	return nullptr;
}

HDF5Object HDF5DatasetSelect::intersection(HDF5Object const& obj1,
                                           HDF5Object const& obj2, bool& ok)
{
	HDF5Object result;
	if(obj1.name != obj2.name || obj1.type != obj2.type)
	{
		ok = false;
		return result;
	}

	result.name = obj1.name;
	result.type = obj1.type;

	for(auto const& link1 : obj1.links)
	{
		for(auto const& link2 : obj2.links)
		{
			HDF5Object link(intersection(link1, link2, ok));
			if(ok)
			{
				result.links.push_back(link);
			}
		}
	}
	ok = true;
	return result;
}
