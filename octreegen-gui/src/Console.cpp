/*
    Copyright (C) 2024 Florian Cabot <florian.cabot@hotmail.fr>

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#include "Console.hpp"

#include <QCoreApplication>
#include <QHBoxLayout>
#include <QLabel>
#include <QMessageBox>
#include <QProcess>
#include <QPushButton>
#include <QScrollBar>
#include <QTextEdit>
#include <QVBoxLayout>

#include "utils.hpp"

void Console::run(QString const& program, QStringList const& arguments)
{
	Console console(program, arguments);
	console.exec();
}

void processCarriageReturns(QString& str)
{
	int cr = INT_MIN;
	while((cr = str.indexOf('\r')) >= 0)
	{
		int lineBreak = str.lastIndexOf('\n', cr);
		// + 3 := remove "\r\033[K" after '\r'
		str.remove(lineBreak + 1, cr - lineBreak + 3);
	}
}

Console::Console(QString const& program, QStringList const& arguments)
{
	setModal(true);

	auto* layout = make_qt_unique<QVBoxLayout>(*this);

	auto* te = make_qt_unique<QTextEdit>(*this);
	te->setReadOnly(true);
	te->setFont(QFont("Monospace", 10));
	te->setFixedSize(QSize(1200, 800));
	te->setStyleSheet("QTextEdit {background-color:black;color:white;} ");
	te->ensureCursorVisible();

	auto* statusBar = make_qt_unique<QWidget>(*this);
	auto* sbLayout  = make_qt_unique<QHBoxLayout>(*statusBar);

	auto* statusText = make_qt_unique<QLabel>(*this);
	statusText->setText("Running...");
	auto* closeButton = make_qt_unique<QPushButton>(*this);
	closeButton->setEnabled(false);
	closeButton->setText(tr("Close"));
	connect(closeButton, &QPushButton::clicked, this, &Console::accept);

	sbLayout->addWidget(statusText);
	sbLayout->addWidget(closeButton);

	layout->addWidget(te);
	layout->addWidget(statusBar);

	show();
	setFixedSize(this->size());

	QProcess proc(this);
	proc.start(program, arguments);
	if(!proc.waitForStarted())
	{
		statusText->setText(tr("CRITICAL: Could not start \"%1\". Please make "
		                       "sure it is installed.")
		                        .arg(program));
		proc.waitForFinished();
		closeButton->setEnabled(true);
		return;
	}

	auto cmdLine = program;
	for(auto const& arg : arguments)
	{
		cmdLine += ' ' + arg;
	}
	QString detailed("Command :\n" + cmdLine + "\n\nOutput :\n");

	while(!proc.waitForFinished(100))
	{
		QCoreApplication::processEvents();
		detailed += proc.readAll().toStdString().c_str();
		processCarriageReturns(detailed);
		te->setText(detailed);
		te->verticalScrollBar()->setValue(te->verticalScrollBar()->maximum());
	}
	detailed += proc.readAll().toStdString().c_str();
	processCarriageReturns(detailed);
	te->setText(detailed);
	te->verticalScrollBar()->setValue(te->verticalScrollBar()->maximum());
	if(proc.exitCode() != 0)
	{
		statusText->setText(tr("WARNING: Something went wrong..."));
		closeButton->setEnabled(true);
		return;
	}
	statusText->setText(tr("Success !"));
	closeButton->setEnabled(true);
}
