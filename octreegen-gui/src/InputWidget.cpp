/*
    Copyright (C) 2024 Florian Cabot <florian.cabot@hotmail.fr>

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#include "InputWidget.hpp"

#include <QDoubleSpinBox>
#include <QLabel>
#include <QTabWidget>
#include <QVBoxLayout>

#include "inputwidgets/HDF5InputWidget.hpp"
#include "inputwidgets/OctreeInputWidget.hpp"
#include "inputwidgets/RandomInputWidget.hpp"
#include "utils.hpp"

InputWidget::InputWidget(QWidget* parent)
    : CommandInputParametersWidget(parent)
    , sampleRateSpinBox(make_qt_unique<QDoubleSpinBox>(*this))
    , inputTypeSelector(make_qt_unique<QTabWidget>(*this))
{
	auto* mainLayout = make_qt_unique<QVBoxLayout>(*this);

	mainLayout->addWidget(make_qt_unique<QLabel>(*this, "INPUT"));

	auto* sampleRateWidget = make_qt_unique<QWidget>(*this);
	auto* sampleRateLayout = make_qt_unique<QHBoxLayout>(*sampleRateWidget);
	sampleRateLayout->addWidget(
	    make_qt_unique<QLabel>(*sampleRateWidget, tr("Sample rate:")));
	sampleRateLayout->addWidget(sampleRateSpinBox);

	mainLayout->addWidget(sampleRateWidget);

	auto* randomInputWidget
	    = make_qt_unique<RandomInputWidget>(*inputTypeSelector);
	inputTypeSelector->addTab(randomInputWidget, tr("Random"));
	auto* octreeInputWidget
	    = make_qt_unique<OctreeInputWidget>(*inputTypeSelector);
	inputTypeSelector->addTab(octreeInputWidget, tr("Octree"));
	auto* hdf5InputWidget = make_qt_unique<HDF5InputWidget>(*inputTypeSelector);
	inputTypeSelector->addTab(hdf5InputWidget, tr("HDF5"));

	mainLayout->addWidget(inputTypeSelector);

	sampleRateSpinBox->setRange(0.0, 1.0);
	sampleRateSpinBox->setDecimals(3);
	sampleRateSpinBox->setSingleStep(0.001);
	sampleRateSpinBox->setValue(1.0);

	connect(inputTypeSelector, &QTabWidget::currentChanged,
	        [this]() {
		        setOutputAutoPath(getCurrentParamWidget().getOutputAutoPath());
	        });
	connect(inputTypeSelector, &QTabWidget::currentChanged, this,
	        &InputWidget::evaluateValid);

	connect(randomInputWidget, &RandomInputWidget::outputAutoPathChanged,
	        [this](QString const& outputAutoPath)
	        { setOutputAutoPath(outputAutoPath); });
	connect(randomInputWidget, &RandomInputWidget::validityChanged,
	        [this](bool validity) { setValidity(validity); });
	connect(octreeInputWidget, &OctreeInputWidget::outputAutoPathChanged,
	        [this](QString const& outputAutoPath)
	        { setOutputAutoPath(outputAutoPath); });
	connect(octreeInputWidget, &OctreeInputWidget::validityChanged,
	        [this](bool validity) { setValidity(validity); });
	connect(hdf5InputWidget, &HDF5InputWidget::outputAutoPathChanged,
	        [this](QString const& outputAutoPath)
	        { setOutputAutoPath(outputAutoPath); });
	connect(hdf5InputWidget, &HDF5InputWidget::validityChanged,
	        [this](bool validity) { setValidity(validity); });

	setOutputAutoPath(getCurrentParamWidget().getOutputAutoPath());
	evaluateValid();
}

QStringList InputWidget::generateArguments() const
{
	QStringList result;
	result << "--sample-rate=" + QString::number(sampleRateSpinBox->value());

	for(auto const& arg : getCurrentParamWidget().generateArguments())
	{
		result << arg;
	}

	return result;
}

void InputWidget::evaluateValid()
{
	getCurrentParamWidget().evaluateValid();
}

CommandInputParametersWidget const& InputWidget::getCurrentParamWidget() const
{
	return dynamic_cast<CommandInputParametersWidget const&>(
	    *inputTypeSelector->currentWidget());
}

CommandInputParametersWidget& InputWidget::getCurrentParamWidget()
{
	return dynamic_cast<CommandInputParametersWidget&>(
	    *inputTypeSelector->currentWidget());
}
