/*
    Copyright (C) 2024 Florian Cabot <florian.cabot@hotmail.fr>

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#include "inputwidgets/OctreeInputWidget.hpp"

#include <QFileDialog>
#include <QLabel>
#include <QLineEdit>
#include <QPushButton>
#include <QRegularExpression>
#include <QVBoxLayout>

#include "utils.hpp"

OctreeInputWidget::OctreeInputWidget(QWidget* parent)
    : CommandInputParametersWidget(parent)
    , filesEdit(make_qt_unique<QLineEdit>(*this))
    , invalidityLabel(make_qt_unique<QLabel>(*this, ""))
{
	auto* mainLayout = make_qt_unique<QVBoxLayout>(*this);

	auto* fileSelector = make_qt_unique<QWidget>(*this);
	mainLayout->addWidget(fileSelector);
	mainLayout->addWidget(invalidityLabel);

	auto* fileSelectorLayout = make_qt_unique<QHBoxLayout>(*fileSelector);
	fileSelectorLayout->addWidget(
	    make_qt_unique<QLabel>(*fileSelector, tr("Octree file(s):")));
	fileSelectorLayout->addWidget(filesEdit);
	auto* button = make_qt_unique<QPushButton>(*fileSelector, tr("..."));
	fileSelectorLayout->addWidget(button);

	filesEdit->setReadOnly(true);
	connect(filesEdit, &QLineEdit::textChanged, this,
	        &OctreeInputWidget::evaluateValid);
	connect(filesEdit, &QLineEdit::textChanged, this,
	        &OctreeInputWidget::computeOutputAutoPath);
	connect(button, &QPushButton::clicked, this,
	        &OctreeInputWidget::selectFiles);

	invalidityLabel->setStyleSheet("QLabel { color : red; }");

	computeOutputAutoPath();
}

QStringList OctreeInputWidget::generateArguments() const
{
	QStringList args;
	args << "--input-octree";
	for(auto const& path : filesEdit->text().split(';'))
	{
		args << path;
	}
	return args;
}

void OctreeInputWidget::evaluateValid()
{
	if(filesEdit->text().isEmpty())
	{
		invalidityLabel->setText(tr("No input file selected"));
	}
	else
	{
		invalidityLabel->setText("");
		setValidity(true);
		return;
	}
	setValidity(false);
}

void OctreeInputWidget::selectFiles()
{
	QStringList result = QFileDialog::getOpenFileNames(
	    this, tr("Open Octree File(s)"), {}, tr("Octree files (*.octree)"));
	if(!result.isEmpty())
	{
		filesEdit->setText(result.join(';'));
	}
}

void OctreeInputWidget::computeOutputAutoPath()
{
	if(filesEdit->text().isEmpty())
	{
		setOutputAutoPath("");
	}

	QString commonPart(filesEdit->text().split(';')[0]);

	for(auto const& path : filesEdit->text().split(';'))
	{
		for(int i(0); i < commonPart.size() && i < path.size(); ++i)
		{
			if(commonPart[i] != path[i])
			{
				commonPart = commonPart.left(i);
			}
		}
	}

	QFileInfo fi(commonPart);
	QString outputAutoPath(fi.absoluteDir().path() + "/output.octree");
	int number = 1;

	while(QFileInfo::exists(outputAutoPath))
	{
		outputAutoPath = fi.absoluteDir().path() + "/output_"
		                 + QString::number(number) + ".octree";
		number++;
	}

	setOutputAutoPath(outputAutoPath);
}
