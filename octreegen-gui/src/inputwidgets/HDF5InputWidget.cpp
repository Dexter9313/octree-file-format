/*
    Copyright (C) 2024 Florian Cabot <florian.cabot@hotmail.fr>

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#include "inputwidgets/HDF5InputWidget.hpp"

#include <QCheckBox>
#include <QFileDialog>
#include <QLabel>
#include <QLineEdit>
#include <QPushButton>
#include <QVBoxLayout>

#include "HDF5DatasetSelect.hpp"
#include "utils.hpp"

HDF5InputWidget::HDF5InputWidget(QWidget* parent)
    : CommandInputParametersWidget(parent)
    , filesEdit(make_qt_unique<QLineEdit>(*this))
    , coordinatesSelect(make_qt_unique<HDF5DatasetSelect>(
          *this, tr("Select particles coordinates:"), 3))
    , radiusSelectCB(make_qt_unique<QCheckBox>(*this, tr("Specify radius")))
    , radiusSelect(make_qt_unique<HDF5DatasetSelect>(
          *this, tr("Select particles radius:"), 1))
    , luminositySelectCB(
          make_qt_unique<QCheckBox>(*this, tr("Specify luminosity")))
    , luminositySelect(make_qt_unique<HDF5DatasetSelect>(
          *this, tr("Select particles luminosity:"), 1))
    , luminosityRgbSelectCB(
          make_qt_unique<QCheckBox>(*this, tr("Specify RGB luminosity")))
    , luminosityRgbSelect(make_qt_unique<HDF5DatasetSelect>(
          *this, tr("Select particles RGB luminosity:"), 3))
    , densitySelectCB(make_qt_unique<QCheckBox>(*this, tr("Specify density")))
    , densitySelect(make_qt_unique<HDF5DatasetSelect>(
          *this, tr("Select particles density:"), 1))
    , temperatureSelectCB(
          make_qt_unique<QCheckBox>(*this, tr("Specify temperature")))
    , temperatureSelect(make_qt_unique<HDF5DatasetSelect>(
          *this, tr("Select particles temperature"), 1))
    , invalidityLabel(make_qt_unique<QLabel>(*this, ""))
{
	auto* mainLayout = make_qt_unique<QVBoxLayout>(*this);

	auto* fileSelector = make_qt_unique<QWidget>(*this);
	mainLayout->addWidget(fileSelector);
	mainLayout->addWidget(radiusSelectCB);
	mainLayout->addWidget(luminositySelectCB);
	mainLayout->addWidget(luminosityRgbSelectCB);
	mainLayout->addWidget(densitySelectCB);
	mainLayout->addWidget(temperatureSelectCB);
	auto* datasetSelectors = make_qt_unique<QWidget>(*this);
	mainLayout->addWidget(datasetSelectors);
	mainLayout->addWidget(invalidityLabel);

	auto* fileSelectorLayout = make_qt_unique<QHBoxLayout>(*fileSelector);
	fileSelectorLayout->addWidget(
	    make_qt_unique<QLabel>(*fileSelector, tr("HDF5 file(s):")));
	fileSelectorLayout->addWidget(filesEdit);
	auto* button = make_qt_unique<QPushButton>(*fileSelector, tr("..."));
	fileSelectorLayout->addWidget(button);

	auto* datasetSelectorsLayout
	    = make_qt_unique<QHBoxLayout>(*datasetSelectors);
	datasetSelectorsLayout->addWidget(coordinatesSelect);
	datasetSelectorsLayout->addWidget(radiusSelect);
	datasetSelectorsLayout->addWidget(luminositySelect);
	datasetSelectorsLayout->addWidget(luminosityRgbSelect);
	datasetSelectorsLayout->addWidget(densitySelect);
	datasetSelectorsLayout->addWidget(temperatureSelect);

	filesEdit->setReadOnly(true);
	connect(filesEdit, &QLineEdit::textChanged,
	        [this](QString const& newText)
	        {
		        evaluateValid();
		        computeOutputAutoPath();
		        coordinatesSelect->load(newText.split(';'));
		        radiusSelect->load(newText.split(';'));
		        luminositySelect->load(newText.split(';'));
		        luminosityRgbSelect->load(newText.split(';'));
		        densitySelect->load(newText.split(';'));
		        temperatureSelect->load(newText.split(';'));
	        });
	connect(button, &QPushButton::clicked, this, &HDF5InputWidget::selectFiles);

	connect(coordinatesSelect, &HDF5DatasetSelect::selectedObjChanged,
	        [this]()
	        {
		        evaluateValid();
		        computeOutputAutoPath();
	        });
	radiusSelectCB->setChecked(false);
	connect(radiusSelectCB, &QCheckBox::stateChanged,
	        [this](int state)
	        {
		        radiusSelect->setVisible(state != Qt::CheckState::Unchecked);
		        evaluateValid();
	        });
	luminositySelectCB->setChecked(false);
	connect(luminositySelectCB, &QCheckBox::stateChanged,
	        [this](int state)
	        {
		        luminositySelect->setVisible(state
		                                     != Qt::CheckState::Unchecked);
		        evaluateValid();
	        });
	luminosityRgbSelectCB->setChecked(false);
	connect(luminosityRgbSelectCB, &QCheckBox::stateChanged,
	        [this](int state)
	        {
		        luminosityRgbSelect->setVisible(state
		                                        != Qt::CheckState::Unchecked);
		        evaluateValid();
	        });
	densitySelectCB->setChecked(false);
	connect(densitySelectCB, &QCheckBox::stateChanged,
	        [this](int state)
	        {
		        densitySelect->setVisible(state != Qt::CheckState::Unchecked);
		        evaluateValid();
	        });
	temperatureSelectCB->setChecked(false);
	connect(temperatureSelectCB, &QCheckBox::stateChanged,
	        [this](int state)
	        {
		        temperatureSelect->setVisible(state
		                                      != Qt::CheckState::Unchecked);
		        evaluateValid();
	        });
	radiusSelect->hide();
	connect(radiusSelect, &HDF5DatasetSelect::selectedObjChanged,
	        [this]() { evaluateValid(); });
	luminositySelect->hide();
	connect(luminositySelect, &HDF5DatasetSelect::selectedObjChanged,
	        [this]() { evaluateValid(); });
	luminosityRgbSelect->hide();
	connect(luminosityRgbSelect, &HDF5DatasetSelect::selectedObjChanged,
	        [this]() { evaluateValid(); });
	densitySelect->hide();
	connect(densitySelect, &HDF5DatasetSelect::selectedObjChanged,
	        [this]() { evaluateValid(); });
	temperatureSelect->hide();
	connect(temperatureSelect, &HDF5DatasetSelect::selectedObjChanged,
	        [this]() { evaluateValid(); });

	invalidityLabel->setStyleSheet("QLabel { color : red; }");

	computeOutputAutoPath();
}

QStringList HDF5InputWidget::generateArguments() const
{
	QStringList args;
	args << "--input-hdf5";
	for(auto const& path : filesEdit->text().split(';'))
	{
		args << path;
	}
	args << "--coord-path=" + coordinatesSelect->getDatasetPath();
	if(radiusSelectCB->checkState() != Qt::CheckState::Unchecked)
	{
		args << "--radius-path=" + radiusSelect->getDatasetPath();
	}
	if(luminositySelectCB->checkState() != Qt::CheckState::Unchecked)
	{
		args << "--lum-path=" + luminositySelect->getDatasetPath();
	}
	if(luminosityRgbSelectCB->checkState() != Qt::CheckState::Unchecked)
	{
		args << "--rgb-lum-path=" + luminosityRgbSelect->getDatasetPath();
	}
	if(densitySelectCB->checkState() != Qt::CheckState::Unchecked)
	{
		args << "--density-path=" + densitySelect->getDatasetPath();
	}
	if(temperatureSelectCB->checkState() != Qt::CheckState::Unchecked)
	{
		args << "--temperature-path=" + temperatureSelect->getDatasetPath();
	}
	return args;
}

void HDF5InputWidget::evaluateValid()
{
	if(filesEdit->text().isEmpty())
	{
		invalidityLabel->setText(tr("No input file selected"));
	}
	else if(!coordinatesSelect->datasetPathIsValid()
	        || (radiusSelectCB->checkState() != Qt::CheckState::Unchecked
	            && !radiusSelect->datasetPathIsValid())
	        || (luminositySelectCB->checkState() != Qt::CheckState::Unchecked
	            && !luminositySelect->datasetPathIsValid())
	        || (luminosityRgbSelectCB->checkState() != Qt::CheckState::Unchecked
	            && !luminosityRgbSelect->datasetPathIsValid())
	        || (densitySelectCB->checkState() != Qt::CheckState::Unchecked
	            && !densitySelect->datasetPathIsValid())
	        || (temperatureSelectCB->checkState() != Qt::CheckState::Unchecked
	            && !temperatureSelect->datasetPathIsValid()))
	{
		invalidityLabel->setText(tr(""));
	}
	else
	{
		invalidityLabel->setText("");
		setValidity(true);
		return;
	}
	setValidity(false);
}

void HDF5InputWidget::selectFiles()
{
	QStringList result = QFileDialog::getOpenFileNames(
	    this, tr("Open HDF5 File(s)"), {}, tr("HDF5 files (*.h5; *.hdf5)"));
	if(!result.isEmpty())
	{
		filesEdit->setText(result.join(';'));
	}
}

void HDF5InputWidget::computeOutputAutoPath()
{
	if(filesEdit->text().isEmpty())
	{
		setOutputAutoPath("");
	}

	QString datasetPath(coordinatesSelect->getDatasetPath());
	QStringList splittedPath = datasetPath.split('/');
	splittedPath.pop_back();

	QFileInfo fi(filesEdit->text().split(';')[0]);
	auto groupName = coordinatesSelect->datasetPathIsValid()
	                     ? '.' + splittedPath.last()
	                     : QString{};
	setOutputAutoPath(fi.dir().absolutePath() + QDir::separator()
	                  + fi.baseName() + groupName + ".octree");
}
