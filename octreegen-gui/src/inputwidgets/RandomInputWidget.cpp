/*
    Copyright (C) 2024 Florian Cabot <florian.cabot@hotmail.fr>

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#include "inputwidgets/RandomInputWidget.hpp"

#include <QCheckBox>
#include <QDir>
#include <QLabel>
#include <QLineEdit>
#include <QVBoxLayout>
#include <QValidator>

#include "utils.hpp"

class LongIntValidator : public QValidator
{
  public:
	explicit LongIntValidator(QObject* parent = nullptr)
	    : QValidator(parent){};
	void fixup(QString& input) const override
	{
		QString result;
		for(auto c : input)
		{
			if(c >= '0' && c <= '9')
			{
				result += c;
			}
		}
		input = result;
		if(input.isEmpty())
		{
			input = '0';
		}
		while(input.size() > 1 && input[0] == '0')
		{
			input = input.right(1);
		}
	};
	QValidator::State validate(QString& input, int& /*pos*/) const override
	{
		if(input.isEmpty())
		{
			return QValidator::State::Intermediate;
		}
		for(auto c : input)
		{
			if(c < '0' || c > '9')
			{
				return QValidator::State::Invalid;
			}
		}
		if(input.size() > 1 && input[0] == '0')
		{
			return QValidator::State::Invalid;
		}
		return QValidator::State::Acceptable;
	};
};

RandomInputWidget::RandomInputWidget(QWidget* parent)
    : CommandInputParametersWidget(parent)
    , particlesNumber(make_qt_unique<QLineEdit>(*this))
    , addRadiusCB(make_qt_unique<QCheckBox>(*this, tr("Add radius dimension")))
    , addLumCB(make_qt_unique<QCheckBox>(*this,
                                         tr("Add total luminosity dimension")))
    , addLumRgbCB(
          make_qt_unique<QCheckBox>(*this, tr("Add RGB luminosity dimensions")))
    , addDensityCB(
          make_qt_unique<QCheckBox>(*this, tr("Add density dimension")))
    , addTemperatureCB(
          make_qt_unique<QCheckBox>(*this, tr("Add temperature dimension")))
    , invalidityLabel(make_qt_unique<QLabel>(*this, ""))
{
	auto* mainLayout = make_qt_unique<QVBoxLayout>(*this);

	mainLayout->addWidget(particlesNumber);
	mainLayout->addWidget(addRadiusCB);
	mainLayout->addWidget(addLumCB);
	mainLayout->addWidget(addLumRgbCB);
	mainLayout->addWidget(addDensityCB);
	mainLayout->addWidget(addTemperatureCB);
	mainLayout->addWidget(invalidityLabel);

	connect(particlesNumber, &QLineEdit::textChanged,
	        [this](QString const& text)
	        {
		        auto currentDir = QDir::currentPath();
		        setOutputAutoPath(currentDir + '/' + "random." + text
		                          + ".octree");
		        evaluateValid();
	        });
	particlesNumber->setValidator(
	    make_qt_unique<LongIntValidator>(*particlesNumber));
	particlesNumber->setText("1000000");

	invalidityLabel->setStyleSheet("QLabel { color : red; }");
}

QStringList RandomInputWidget::generateArguments() const
{
	QStringList args;
	args << "--input-random";
	args << particlesNumber->text();
	if(addRadiusCB->isChecked())
	{
		args << "--add-radius";
	}
	if(addLumCB->isChecked())
	{
		args << "--add-lum";
	}
	if(addLumRgbCB->isChecked())
	{
		args << "--add-rgb-lum";
	}
	if(addDensityCB->isChecked())
	{
		args << "--add-density";
	}
	if(addTemperatureCB->isChecked())
	{
		args << "--add-temperature";
	}
	return args;
}

void RandomInputWidget::evaluateValid()
{
	auto text = particlesNumber->text();
	if(text.isEmpty() || text == "0")
	{
		invalidityLabel->setText(tr("No particles will be generated"));
	}
	else if(text.size() >= 10 && (text[0] != '1' || text != "1000000000"))
	{
		invalidityLabel->setText(
		    tr("Max random particles is 1 billion (1000000000)"));
	}
	else
	{
		invalidityLabel->setText("");
		setValidity(true);
		return;
	}
	setValidity(false);
}
