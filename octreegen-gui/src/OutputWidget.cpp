/*
    Copyright (C) 2024 Florian Cabot <florian.cabot@hotmail.fr>

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#include "OutputWidget.hpp"

#include <QCheckBox>
#include <QFileDialog>
#include <QHBoxLayout>
#include <QLabel>
#include <QLineEdit>
#include <QPushButton>
#include <QSpinBox>
#include <QVBoxLayout>

#include "utils.hpp"

OutputWidget::OutputWidget(QWidget* parent)
    : CommandParametersWidget(parent)
    , disableNodeNormCB(
          make_qt_unique<QCheckBox>(*this, tr("Disable Node Normalization")))
    , useZSTDCB(make_qt_unique<QCheckBox>(*this, tr("Use ZSTD Compression")))
    , maxParticlesPerNodeSB(make_qt_unique<QSpinBox>(*this))
    , autoCheckBox(make_qt_unique<QCheckBox>(*this, tr("auto")))
    , lineEditSaveAs(make_qt_unique<QLineEdit>(*this))
    , invalidityLabel(make_qt_unique<QLabel>(*this, ""))
{
	auto* mainLayout = make_qt_unique<QVBoxLayout>(*this);

	mainLayout->addWidget(make_qt_unique<QLabel>(*this, "OUTPUT"));

	auto* saveAsWidget = make_qt_unique<QWidget>(*this);
	auto* saveAsLayout = make_qt_unique<QHBoxLayout>(*saveAsWidget);
	saveAsLayout->addWidget(
	    make_qt_unique<QLabel>(*saveAsWidget, tr("Save as:")));
	saveAsLayout->addWidget(autoCheckBox);
	saveAsLayout->addWidget(lineEditSaveAs);
	auto* button = make_qt_unique<QPushButton>(*saveAsWidget, tr("..."));
	saveAsLayout->addWidget(button);

	mainLayout->addWidget(disableNodeNormCB);
	mainLayout->addWidget(useZSTDCB);

	auto* maxPartWidget = make_qt_unique<QWidget>(*this);
	auto* maxPartLayout = make_qt_unique<QHBoxLayout>(*maxPartWidget);
	maxPartLayout->addWidget(
	    make_qt_unique<QLabel>(*maxPartWidget, tr("Max Particles Per Node: ")));
	maxPartLayout->addWidget(maxParticlesPerNodeSB);

	mainLayout->addWidget(maxPartWidget);
	mainLayout->addWidget(saveAsWidget);
	mainLayout->addWidget(invalidityLabel);

	maxParticlesPerNodeSB->setRange(0, 1000000000);
	maxParticlesPerNodeSB->setSingleStep(1000);
	maxParticlesPerNodeSB->setValue(16000);
	autoCheckBox->setChecked(true);
	lineEditSaveAs->setEnabled(false);
	button->setEnabled(false);
	connect(autoCheckBox, &QCheckBox::stateChanged,
	        [this, button](int state)
	        {
		        lineEditSaveAs->setEnabled(state == Qt::CheckState::Unchecked);
		        button->setEnabled(state == Qt::CheckState::Unchecked);
		        if(state != Qt::CheckState::Unchecked)
		        {
			        lineEditSaveAs->setText(outputAutoPath);
		        }
	        });
	connect(lineEditSaveAs, &QLineEdit::textChanged, this,
	        &OutputWidget::evaluateValid);
	connect(button, &QPushButton::clicked, this, &OutputWidget::selectSaveAs);

	invalidityLabel->setStyleSheet("QLabel { color : red; }");

	evaluateValid();
}

QStringList OutputWidget::generateArguments() const
{
	QStringList args("--output");
	if(disableNodeNormCB->isChecked())
	{
		args << "--disable-node-normalization";
	}
	if(useZSTDCB->isChecked())
	{
		args << "--use-zstd-compression";
	}
	args << "--max-particles-per-node="
	            + QString::number(maxParticlesPerNodeSB->value());
	args << lineEditSaveAs->text();
	return args;
}

void OutputWidget::setOutputAutoPath(QString const& path)
{
	outputAutoPath = path;
	if(!lineEditSaveAs->isEnabled())
	{
		lineEditSaveAs->setText(path);
	}
}

void OutputWidget::evaluateValid()
{
	QFileInfo fi(lineEditSaveAs->text());
	if(lineEditSaveAs->text().isEmpty())
	{
		invalidityLabel->setText(tr("Output path is empty"));
	}
	else if(fi.isDir())
	{
		invalidityLabel->setText(tr("Output path is a directory"));
	}
	else if(!fi.absoluteDir().exists())
	{
		invalidityLabel->setText(
		    tr("Output path is within a non-existing directory"));
	}
	else
	{
		setValidity(true);
		invalidityLabel->setText("");
		return;
	}

	setValidity(false);
}

void OutputWidget::selectSaveAs()
{
	QString result = QFileDialog::getSaveFileName(
	    this, tr("Save as"), lineEditSaveAs->text(),
	    tr("Octree files (*.octree)"));
	if(result != "")
	{
		lineEditSaveAs->setText(result);
	}
}
